# Contributor: Patrycja Rosa <alpine@ptrcnull.me>
# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-findpython
pkgver=0.6.0
pkgrel=1
pkgdesc="Utility to find python versions on your system"
url="https://github.com/frostming/findpython"
arch="noarch"
license="MIT"
depends="py3-packaging"
makedepends="py3-gpep517 py3-installer py3-pdm-backend"
checkdepends="py3-pytest"
subpackages="$pkgname-pyc"
source="https://github.com/frostming/findpython/archive/$pkgver/py3-findpython-$pkgver.tar.gz"
builddir="$srcdir/findpython-$pkgver"

build() {
	export PDM_BUILD_SCM_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
e6651fe040042ca1f7b75fd8fed899be91fa5d3e51b898ff6e9cb59a0d0155a9cdc2af26321681d51326b7f7900c1048bc813aad7ad201269b8bb5a2775427c2  py3-findpython-0.6.0.tar.gz
"
