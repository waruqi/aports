# Contributor: Antoine Martin (ayakael) <dev@ayakael.net>
# Maintainer: Antoine Martin (ayakael) <dev@ayakael.net>
pkgname=py3-python-ipware
_pyname="python-ipware"
pkgver=2.0.4
pkgrel=0
arch="noarch"
pkgdesc="A python package for server applications to retrieve client's IP address"
url="https://pypi.python.org/project/python-ipware"
license="MIT"
makedepends="
	py3-setuptools
	py3-gpep517
	py3-wheel
	"
checkdepends="py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://github.com/un33k/python-ipware/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir"/$_pyname-$pkgver
subpackages="$pkgname-pyc"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m unittest discover
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
bffa9da4693ecc0f30d51d01ce9a6e50f9b22cb8ac429038e146343c156bfb53742959c1c3aa3e395ad8cfb966d3ba4e9734d6a2304beb723e8cb1177d3a705e  py3-python-ipware-2.0.4.tar.gz
"
